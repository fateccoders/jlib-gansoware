package br.gov.sp.fatec.gansoware.dominio;

public class FTCGWEntidadeDominio implements FTCGWIEntidade {

    private long ftgcw_id = 0L;

    public long getFtgcw_id() {
        return ftgcw_id;
    }

    public void setFtgcw_id(long ftgcw_id) {
        this.ftgcw_id = ftgcw_id;
    }
}

package br.gov.sp.fatec.gansoware.dominio;

public class FTCGWGraduando extends FTCGWPresente {

    private String ftcgw_ra = null;

    public String getFtcgw_ra() {
        return ftcgw_ra;
    }

    public void setFtcgw_ra(String ftcgw_ra) {
        this.ftcgw_ra = ftcgw_ra;
    }

    public FTCGWGraduando() {
    }
}

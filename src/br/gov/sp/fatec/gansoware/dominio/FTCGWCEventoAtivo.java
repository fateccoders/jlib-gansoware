package br.gov.sp.fatec.gansoware.dominio;

public abstract class FTCGWCEventoAtivo extends FTCGWEntidadeDominio {

    protected String ftcgw_localizacao = null;
    protected short ftcgw_durabilidade = 0;
    protected String ftcgw_ocasiao = null;

    public abstract String getFtcgw_localizacao();

    public abstract void setFtcgw_localizacao(String ftcgw_localizacao);

    public abstract short getFtcgw_durabilidade();

    public abstract void setFtcgw_durabilidade(short ftcgw_durabilidade);

    public abstract String getFtcgw_ocasiao();

    public abstract void setFtcgw_ocasiao(String ftcgw_ocasiao);
}

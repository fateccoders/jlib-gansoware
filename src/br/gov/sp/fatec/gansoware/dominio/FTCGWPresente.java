package br.gov.sp.fatec.gansoware.dominio;

public class FTCGWPresente extends FTCGWCPresente {

    public String getFtcgw_nome() {
        return ftcgw_nome;
    }

    public void setFtcgw_nome(String ftcgw_nome) {
        this.ftcgw_nome = ftcgw_nome;
    }

    public byte getFtcgw_sexo() {
        return ftcgw_sexo;
    }

    public void setFtcgw_sexo(byte ftcgw_sexo) {
        this.ftcgw_sexo = ftcgw_sexo;
    }

    public short getFtcgw_idade() {
        return ftcgw_idade;
    }

    public void setFtcgw_idade(short ftcgw_idade) {
        this.ftcgw_idade = ftcgw_idade;
    }

    @Override
    public void ftcgwPromoverMinhaVisibilidade(FTCGWPalestra ftcgw_comunicar_com_evento) {

    }

    @Override
    public void ftcgwPromoverMinhaDesagregacao(FTCGWPalestra ftcgw_comunicar_com_evento) {

    }

    public FTCGWPresente() {
    }
}

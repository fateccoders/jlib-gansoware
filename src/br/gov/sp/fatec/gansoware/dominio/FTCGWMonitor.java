package br.gov.sp.fatec.gansoware.dominio;

import java.sql.Timestamp;

public class FTCGWMonitor extends FTCGWEntidadeDominio {

    private FTCGWOscilacao ftcgw_computar_oscilacao;
    private Timestamp[] ftcgw_computar_datas_movimento;

    public FTCGWOscilacao getFtcgw_computar_oscilacao() {
        return ftcgw_computar_oscilacao;
    }

    public void setFtcgw_computar_oscilacao(FTCGWOscilacao ftcgw_computar_oscilacao) {
        this.ftcgw_computar_oscilacao = ftcgw_computar_oscilacao;
    }

    public Timestamp[] getFtcgw_computar_datas_movimento() {
        return ftcgw_computar_datas_movimento;
    }

    public void setFtcgw_computar_datas_movimento(Timestamp[] ftcgw_computar_datas_movimento) {
        this.ftcgw_computar_datas_movimento = ftcgw_computar_datas_movimento;
    }

    public FTCGWMonitor() {
    }

}

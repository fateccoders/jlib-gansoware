package br.gov.sp.fatec.gansoware.dominio;

public interface FTCGWIPresente {

    public abstract void ftcgwPromoverMinhaVisibilidade(final FTCGWPalestra ftcgw_comunicar_com_evento);

    public abstract void ftcgwPromoverMinhaDesagregacao(final FTCGWPalestra ftcgw_comunicar_com_evento);
}

package br.gov.sp.fatec.gansoware.dominio;

import java.util.Map;

public class FTCGWPalestra extends FTCGWCEventoAtivo {

    private String ftcgw_titulo = null;
    private boolean ftcgw_execucao = true;
    private FTCGWPalestrante ftcgw_palestrante = null;
    private Map<Long, FTCGWGraduando> ftcgw_graduandos = null;

    @Override
    public String getFtcgw_localizacao() {
        return ftcgw_localizacao;
    }

    @Override
    public void setFtcgw_localizacao(String ftcgw_localizacao) {
        this.ftcgw_localizacao = ftcgw_localizacao;
    }

    @Override
    public short getFtcgw_durabilidade() {
        return ftcgw_durabilidade;
    }

    @Override
    public void setFtcgw_durabilidade(short ftcgw_durabilidade) {
        this.ftcgw_durabilidade = ftcgw_durabilidade;
    }

    @Override
    public String getFtcgw_ocasiao() {
        return ftcgw_ocasiao;
    }

    @Override
    public void setFtcgw_ocasiao(String ftcgw_ocasiao) {
        this.ftcgw_ocasiao = ftcgw_ocasiao;
    }

    public FTCGWPalestrante getFtcgw_palestrante() {
        return ftcgw_palestrante;
    }

    public void setFtcgw_palestrante(FTCGWPalestrante ftcgw_palestrante) {
        this.ftcgw_palestrante = ftcgw_palestrante;
    }

    public Map<Long, FTCGWGraduando> getFtcgw_graduandos() {
        return ftcgw_graduandos;
    }

    public void setFtcgw_graduandos(Map<Long, FTCGWGraduando> ftcgw_graduandos) {
        this.ftcgw_graduandos = ftcgw_graduandos;
    }

    public String getFtcgw_titulo() {
        return ftcgw_titulo;
    }

    public void setFtcgw_titulo(String ftcgw_titulo) {
        this.ftcgw_titulo = ftcgw_titulo;
    }

    public boolean isFtcgw_execucao() {
        return ftcgw_execucao;
    }

    public void setFtcgw_execucao(boolean ftcgw_execucao) {
        this.ftcgw_execucao = ftcgw_execucao;
    }

    public final void ftcgwVisualizarPilhaPalestra() {
    }

    public synchronized Map<Long, FTCGWPresente> ftcgwPromoverMinhaPilhaAtual() {
        return null;
    }

    public final void ftcgwAdministrarPilhaPalestra(char ftcgw_modo_operacional, FTCGWPresente ftcgw_atual_presente) {
    }

    public FTCGWPalestra() {
    }
}

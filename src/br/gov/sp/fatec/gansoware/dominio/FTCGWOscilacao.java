package br.gov.sp.fatec.gansoware.dominio;

public class FTCGWOscilacao extends FTCGWEntidadeDominio {

    private FTCGWGraduando ftcw_associacao_nivelar_aluno;
    private FTCGWPalestra ftcgw_associacao_nivelar_palestra;

    public FTCGWGraduando getFtcw_associacao_nivelar_aluno() {
        return ftcw_associacao_nivelar_aluno;
    }

    public void setFtcw_associacao_nivelar_aluno(FTCGWGraduando ftcw_associacao_nivelar_aluno) {
        this.ftcw_associacao_nivelar_aluno = ftcw_associacao_nivelar_aluno;
    }

    public FTCGWPalestra getFtcgw_associacao_nivelar_palestra() {
        return ftcgw_associacao_nivelar_palestra;
    }

    public void setFtcgw_associacao_nivelar_palestra(FTCGWPalestra ftcgw_associacao_nivelar_palestra) {
        this.ftcgw_associacao_nivelar_palestra = ftcgw_associacao_nivelar_palestra;
    }

    public FTCGWOscilacao() {
    }
}

package br.gov.sp.fatec.gansoware.dominio;

public class FTCGWPalestrante extends FTCGWPresente {

    private String ftcgw_proposito = null;

    public String getFtcgw_proposito() {
        return ftcgw_proposito;
    }

    public void setFtcgw_proposito(String ftcgw_proposito) {
        this.ftcgw_proposito = ftcgw_proposito;
    }

    public FTCGWPalestrante() {
    }
}

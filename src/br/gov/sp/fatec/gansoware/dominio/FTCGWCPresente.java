package br.gov.sp.fatec.gansoware.dominio;

public abstract class FTCGWCPresente extends FTCGWEntidadeDominio implements FTCGWIPresente {

    protected String ftcgw_nome = null;
    protected byte ftcgw_sexo = 0;
    protected short ftcgw_idade = 0;
}

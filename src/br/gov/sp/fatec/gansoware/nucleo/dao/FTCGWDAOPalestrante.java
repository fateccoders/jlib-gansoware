package br.gov.sp.fatec.gansoware.nucleo.dao;

import br.gov.sp.fatec.gansoware.dominio.FTCGWEntidadeDominio;
import br.gov.sp.fatec.gansoware.dominio.FTCGWPalestrante;
import br.gov.sp.fatec.gansoware.dominio.FTCGWPalestrante;
import br.gov.sp.fatec.gansoware.nucleo.necessario.FTCGWConexaoBase;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class FTCGWDAOPalestrante extends FTCGWDAOAbstrato {

    @Override
    public void ftcgwSalvar(FTCGWEntidadeDominio ftcgw_entidade) throws Exception {

        assert (ftcgw_entidade != null);

        final FTCGWPalestrante ftcgw_foco_palestrante = (FTCGWPalestrante) ftcgw_entidade;
        final StringBuilder ftcgw_query = new StringBuilder();
        PreparedStatement ftcgw_indagar_preparo = null;

        this.ftcgw_conexao.ftcgwPromoverConexao('f');

        ftcgw_query.append("INSERT INTO DFTCGWPalestrante(ftcgwcontrole, ftcgwnome, ftcgwsexo, ");
        ftcgw_query.append("ftcgwidade, ftcgwproposito) VALUES (?, ?, ?, ?, ?)");

        ftcgw_indagar_preparo = this.ftcgw_conexao.getFtcgw_via_conectiva().prepareStatement(ftcgw_query.toString());
        ftcgw_indagar_preparo.setLong(1, ftcgw_foco_palestrante.getFtgcw_id());
        ftcgw_indagar_preparo.setString(2, ftcgw_foco_palestrante.getFtcgw_nome());
        ftcgw_indagar_preparo.setString(3, String.valueOf(ftcgw_foco_palestrante.getFtcgw_sexo()));
        ftcgw_indagar_preparo.setShort(4, ftcgw_foco_palestrante.getFtcgw_idade());
        ftcgw_indagar_preparo.setString(5, ftcgw_foco_palestrante.getFtcgw_proposito());

        final int ftcgw_codoperativo = ftcgw_indagar_preparo.executeUpdate();
        ftcgw_indagar_preparo.close();

        this.ftcgw_conexao.ftcgwFinalizarConexao();
    }

    @Override
    public void ftcgwAlterar(FTCGWEntidadeDominio ftcgw_entidade) throws Exception {

        assert (ftcgw_entidade != null);

        final FTCGWPalestrante ftcgw_foco_palestrante = (FTCGWPalestrante) ftcgw_entidade;
        final StringBuilder ftcgw_query = new StringBuilder();
        PreparedStatement ftcgw_indagar_preparo = null;

        this.ftcgw_conexao.ftcgwPromoverConexao('f');

        ftcgw_query.append("UPDATE DFTCGWPalestrante SET ftcgwnome=?, ftcgwsexo=?, ftcgwidade=?, ");
        ftcgw_query.append("ftcgwproposito=? WHERE ftcgwcontrole=?");

        ftcgw_indagar_preparo = this.ftcgw_conexao.getFtcgw_via_conectiva().prepareStatement(ftcgw_query.toString());
        ftcgw_indagar_preparo.setString(1, ftcgw_foco_palestrante.getFtcgw_nome());
        ftcgw_indagar_preparo.setString(2, String.valueOf(ftcgw_foco_palestrante.getFtcgw_sexo()));
        ftcgw_indagar_preparo.setShort(3, ftcgw_foco_palestrante.getFtcgw_idade());
        ftcgw_indagar_preparo.setString(4, ftcgw_foco_palestrante.getFtcgw_proposito());
        ftcgw_indagar_preparo.setLong(5, ftcgw_foco_palestrante.getFtgcw_id());

        final int ftcgw_codoperativo = ftcgw_indagar_preparo.executeUpdate();
        ftcgw_indagar_preparo.close();

        this.ftcgw_conexao.ftcgwFinalizarConexao();
    }

    @Override
    public void ftcgwExcluir(FTCGWEntidadeDominio ftcgw_entidade) throws Exception {
    }

    @Override
    public List<FTCGWEntidadeDominio> ftcgwConsultar(FTCGWEntidadeDominio ftcgw_entidade) throws Exception {
        assert (ftcgw_entidade != null);

        final FTCGWPalestrante ftcgw_foco_palestrante = (FTCGWPalestrante) ftcgw_entidade;
        final StringBuilder ftcgw_query = new StringBuilder();
        PreparedStatement ftcgw_indagar_preparo = null;

        this.ftcgw_conexao.ftcgwPromoverConexao('f');

        if (ftcgw_foco_palestrante.getFtcgw_nome() == null || ftcgw_foco_palestrante.getFtcgw_nome().length() <= 3 || ftcgw_foco_palestrante.getFtcgw_nome().equals("")) {
            ftcgw_query.append("SELECT * FROM DFTCGWPalestrante");
            ftcgw_indagar_preparo = this.ftcgw_conexao.getFtcgw_via_conectiva().prepareStatement(ftcgw_query.toString());
            final ResultSet ftcgw_indagacao_provisionada = ftcgw_indagar_preparo.executeQuery();

            if (ftcgw_indagacao_provisionada.isBeforeFirst()) {
                final List<FTCGWEntidadeDominio> ftcgw_palestrantes_viabilizados = new ArrayList<>();
                while (ftcgw_indagacao_provisionada.next()) {
                    final FTCGWPalestrante ftcgw_palestrante_ciclico = new FTCGWPalestrante();
                    ftcgw_palestrante_ciclico.setFtcgw_nome(ftcgw_indagacao_provisionada.getString("ftcgwnome"));
                    ftcgw_palestrante_ciclico.setFtcgw_sexo(ftcgw_indagacao_provisionada.getByte("ftcgwsexo"));
                    ftcgw_palestrante_ciclico.setFtcgw_idade(ftcgw_indagacao_provisionada.getShort("ftcgwidade"));
                    ftcgw_palestrante_ciclico.setFtcgw_proposito(ftcgw_indagacao_provisionada.getString("ftcgwproposito"));
                    ftcgw_palestrante_ciclico.setFtgcw_id(ftcgw_indagacao_provisionada.getLong("ftcgwcontrole"));
                    ftcgw_palestrantes_viabilizados.add(ftcgw_palestrante_ciclico);
                }
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return ftcgw_palestrantes_viabilizados;
            } else {
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return null;
            }

        } else {
            ftcgw_query.append("SELECT * FROM DFTCGWPalestrante WHERE ftcgwnome=?");
            ftcgw_indagar_preparo = this.ftcgw_conexao.getFtcgw_via_conectiva().prepareStatement(ftcgw_query.toString());
            ftcgw_indagar_preparo.setString(1, ftcgw_foco_palestrante.getFtcgw_nome());
            final ResultSet ftcgw_indagacao_provisionada = ftcgw_indagar_preparo.executeQuery();

            if (ftcgw_indagacao_provisionada.isBeforeFirst()) {
                final List<FTCGWEntidadeDominio> ftcgw_palestrantes_viabilizados = new ArrayList<>();
                while (ftcgw_indagacao_provisionada.next()) {
                    final FTCGWPalestrante ftcgw_palestrante_ciclico = new FTCGWPalestrante();
                    ftcgw_palestrante_ciclico.setFtcgw_nome(ftcgw_indagacao_provisionada.getString("ftcgwnome"));
                    ftcgw_palestrante_ciclico.setFtcgw_sexo(ftcgw_indagacao_provisionada.getByte("ftcgwsexo"));
                    ftcgw_palestrante_ciclico.setFtcgw_idade(ftcgw_indagacao_provisionada.getShort("ftcgwidade"));
                    ftcgw_palestrante_ciclico.setFtcgw_proposito(ftcgw_indagacao_provisionada.getString("ftcgwproposito"));
                    ftcgw_palestrante_ciclico.setFtgcw_id(ftcgw_indagacao_provisionada.getLong("ftcgwcontrole"));
                    ftcgw_palestrantes_viabilizados.add(ftcgw_palestrante_ciclico);
                }
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return ftcgw_palestrantes_viabilizados;
            } else {
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return null;
            }
        }
    }

    public FTCGWDAOPalestrante(FTCGWConexaoBase ftcgw_conexao) {
        super(ftcgw_conexao);
    }
}

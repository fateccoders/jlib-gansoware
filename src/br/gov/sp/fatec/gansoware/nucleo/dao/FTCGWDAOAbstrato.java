package br.gov.sp.fatec.gansoware.nucleo.dao;

import br.gov.sp.fatec.gansoware.nucleo.FTCGWIDAO;
import br.gov.sp.fatec.gansoware.nucleo.necessario.FTCGWConexaoBase;

public abstract class FTCGWDAOAbstrato implements FTCGWIDAO {

    protected FTCGWConexaoBase ftcgw_conexao;
    protected String ftcgw_tabela;
    protected String ftcgw_codigo_id;

    public FTCGWDAOAbstrato(final FTCGWConexaoBase ftcgw_conexao) {
        this.ftcgw_conexao = ftcgw_conexao;
    }
}

package br.gov.sp.fatec.gansoware.nucleo.dao;

import br.gov.sp.fatec.gansoware.dominio.FTCGWEntidadeDominio;
import br.gov.sp.fatec.gansoware.dominio.FTCGWGraduando;
import br.gov.sp.fatec.gansoware.nucleo.necessario.FTCGWConexaoBase;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class FTCGWDAOGraduando extends FTCGWDAOAbstrato {

    @Override
    public void ftcgwSalvar(FTCGWEntidadeDominio ftcgw_entidade) throws Exception {

        assert (ftcgw_entidade != null);

        final FTCGWGraduando ftcgw_foco_graduando = (FTCGWGraduando) ftcgw_entidade;
        final StringBuilder ftcgw_query = new StringBuilder();
        PreparedStatement ftcgw_indagar_preparo = null;

        this.ftcgw_conexao.ftcgwPromoverConexao('f');

        ftcgw_query.append("INSERT INTO DFTCGWGraduando(ftcgwcontrole, ftcgwnome, ftcgwsexo, ");
        ftcgw_query.append("ftcgwidade, ftcgwra) VALUES (?, ?, ?, ?, ?)");

        ftcgw_indagar_preparo = this.ftcgw_conexao.getFtcgw_via_conectiva().prepareStatement(ftcgw_query.toString());
        ftcgw_indagar_preparo.setLong(1, ftcgw_foco_graduando.getFtgcw_id());
        ftcgw_indagar_preparo.setString(2, ftcgw_foco_graduando.getFtcgw_nome());
        ftcgw_indagar_preparo.setString(3, String.valueOf(ftcgw_foco_graduando.getFtcgw_sexo()));
        ftcgw_indagar_preparo.setShort(4, ftcgw_foco_graduando.getFtcgw_idade());
        ftcgw_indagar_preparo.setString(5, ftcgw_foco_graduando.getFtcgw_ra());

        final int ftcgw_codoperativo = ftcgw_indagar_preparo.executeUpdate();
        ftcgw_indagar_preparo.close();

        this.ftcgw_conexao.ftcgwFinalizarConexao();
    }

    @Override
    public void ftcgwAlterar(FTCGWEntidadeDominio ftcgw_entidade) throws Exception {

        assert (ftcgw_entidade != null);

        final FTCGWGraduando ftcgw_foco_graduando = (FTCGWGraduando) ftcgw_entidade;
        final StringBuilder ftcgw_query = new StringBuilder();
        PreparedStatement ftcgw_indagar_preparo = null;

        this.ftcgw_conexao.ftcgwPromoverConexao('f');

        ftcgw_query.append("UPDATE DFTCGWGraduando SET ftcgwnome=?, ftcgwsexo=?, ftcgwidade=?, ");
        ftcgw_query.append("ftcgwra=? WHERE ftcgwcontrole=?");

        ftcgw_indagar_preparo = this.ftcgw_conexao.getFtcgw_via_conectiva().prepareStatement(ftcgw_query.toString());
        ftcgw_indagar_preparo.setString(1, ftcgw_foco_graduando.getFtcgw_nome());
        ftcgw_indagar_preparo.setString(2, String.valueOf(ftcgw_foco_graduando.getFtcgw_sexo()));
        ftcgw_indagar_preparo.setShort(3, ftcgw_foco_graduando.getFtcgw_idade());
        ftcgw_indagar_preparo.setString(4, ftcgw_foco_graduando.getFtcgw_ra());
        ftcgw_indagar_preparo.setLong(5, ftcgw_foco_graduando.getFtgcw_id());

        final int ftcgw_codoperativo = ftcgw_indagar_preparo.executeUpdate();
        ftcgw_indagar_preparo.close();

        this.ftcgw_conexao.ftcgwFinalizarConexao();
    }

    @Override
    public void ftcgwExcluir(FTCGWEntidadeDominio ftcgw_entidade) throws Exception {
    }

    @Override
    public List<FTCGWEntidadeDominio> ftcgwConsultar(FTCGWEntidadeDominio ftcgw_entidade) throws Exception {
        assert (ftcgw_entidade != null);

        final FTCGWGraduando ftcgw_foco_graduando = (FTCGWGraduando) ftcgw_entidade;
        final StringBuilder ftcgw_query = new StringBuilder();
        PreparedStatement ftcgw_indagar_preparo = null;

        this.ftcgw_conexao.ftcgwPromoverConexao('f');

        if (ftcgw_foco_graduando.getFtcgw_ra() == null || ((ftcgw_foco_graduando.getFtcgw_ra().equals("") || ftcgw_foco_graduando.getFtcgw_ra().length() <= 4)
                && ftcgw_foco_graduando.getFtgcw_id() <= 0)) {
            ftcgw_query.append("SELECT * FROM DFTCGWGraduando");
            ftcgw_indagar_preparo = this.ftcgw_conexao.getFtcgw_via_conectiva().prepareStatement(ftcgw_query.toString());
            final ResultSet ftcgw_indagacao_provisionada = ftcgw_indagar_preparo.executeQuery();

            if (ftcgw_indagacao_provisionada.isBeforeFirst()) {
                final List<FTCGWEntidadeDominio> ftcgw_graduandos_viabilizados = new ArrayList<>();
                while (ftcgw_indagacao_provisionada.next()) {
                    final FTCGWGraduando ftcgw_graduando_ciclico = new FTCGWGraduando();
                    ftcgw_graduando_ciclico.setFtcgw_ra(ftcgw_indagacao_provisionada.getString("ftcgwra"));
                    ftcgw_graduando_ciclico.setFtcgw_nome(ftcgw_indagacao_provisionada.getString("ftcgwnome"));
                    ftcgw_graduando_ciclico.setFtcgw_sexo(ftcgw_indagacao_provisionada.getByte("ftcgwsexo"));
                    ftcgw_graduando_ciclico.setFtcgw_idade(ftcgw_indagacao_provisionada.getShort("ftcgwidade"));
                    ftcgw_graduando_ciclico.setFtgcw_id(ftcgw_indagacao_provisionada.getLong("ftcgwcontrole"));
                    ftcgw_graduandos_viabilizados.add(ftcgw_graduando_ciclico);
                }
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return ftcgw_graduandos_viabilizados;
            } else {
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return null;
            }
        } else if (ftcgw_foco_graduando.getFtgcw_id() > 0) {
            ftcgw_query.append("SELECT * FROM DFTCGWGraduando WHERE ftcgwcontrole=?");
            ftcgw_indagar_preparo = this.ftcgw_conexao.getFtcgw_via_conectiva().prepareStatement(ftcgw_query.toString());
            ftcgw_indagar_preparo.setLong(1, ftcgw_foco_graduando.getFtgcw_id());
            final ResultSet ftcgw_indagacao_provisionada = ftcgw_indagar_preparo.executeQuery();

            if (ftcgw_indagacao_provisionada.isBeforeFirst()) {
                final List<FTCGWEntidadeDominio> ftcgw_graduandos_viabilizados = new ArrayList<>();
                while (ftcgw_indagacao_provisionada.next()) {
                    final FTCGWGraduando ftcgw_graduando_ciclico = new FTCGWGraduando();
                    ftcgw_graduando_ciclico.setFtcgw_ra(ftcgw_indagacao_provisionada.getString("ftcgwra"));
                    ftcgw_graduando_ciclico.setFtcgw_nome(ftcgw_indagacao_provisionada.getString("ftcgwnome"));
                    ftcgw_graduando_ciclico.setFtcgw_sexo(ftcgw_indagacao_provisionada.getByte("ftcgwsexo"));
                    ftcgw_graduando_ciclico.setFtcgw_idade(ftcgw_indagacao_provisionada.getShort("ftcgwidade"));
                    ftcgw_graduando_ciclico.setFtgcw_id(ftcgw_indagacao_provisionada.getLong("ftcgwcontrole"));
                    ftcgw_graduandos_viabilizados.add(ftcgw_graduando_ciclico);
                }
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return ftcgw_graduandos_viabilizados;
            } else {
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return null;
            }
        } else {
            ftcgw_query.append("SELECT * FROM DFTCGWGraduando WHERE ftcgwra=?");
            ftcgw_indagar_preparo = this.ftcgw_conexao.getFtcgw_via_conectiva().prepareStatement(ftcgw_query.toString());
            ftcgw_indagar_preparo.setString(1, ftcgw_foco_graduando.getFtcgw_ra());
            final ResultSet ftcgw_indagacao_provisionada = ftcgw_indagar_preparo.executeQuery();

            if (ftcgw_indagacao_provisionada.isBeforeFirst()) {
                final List<FTCGWEntidadeDominio> ftcgw_graduandos_viabilizados = new ArrayList<>();
                while (ftcgw_indagacao_provisionada.next()) {
                    final FTCGWGraduando ftcgw_graduando_ciclico = new FTCGWGraduando();
                    ftcgw_graduando_ciclico.setFtcgw_ra(ftcgw_indagacao_provisionada.getString("ftcgwra"));
                    ftcgw_graduando_ciclico.setFtcgw_nome(ftcgw_indagacao_provisionada.getString("ftcgwnome"));
                    ftcgw_graduando_ciclico.setFtcgw_sexo(ftcgw_indagacao_provisionada.getByte("ftcgwsexo"));
                    ftcgw_graduando_ciclico.setFtcgw_idade(ftcgw_indagacao_provisionada.getShort("ftcgwidade"));
                    ftcgw_graduando_ciclico.setFtgcw_id(ftcgw_indagacao_provisionada.getLong("ftcgwcontrole"));
                    ftcgw_graduandos_viabilizados.add(ftcgw_graduando_ciclico);
                }
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return ftcgw_graduandos_viabilizados;
            } else {
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return null;
            }
        }
    }

    public FTCGWDAOGraduando(FTCGWConexaoBase ftcgw_conexao) {
        super(ftcgw_conexao);
    }
}

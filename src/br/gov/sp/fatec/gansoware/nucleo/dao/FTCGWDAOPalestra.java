package br.gov.sp.fatec.gansoware.nucleo.dao;

import br.gov.sp.fatec.gansoware.dominio.FTCGWEntidadeDominio;
import br.gov.sp.fatec.gansoware.dominio.FTCGWPalestra;
import br.gov.sp.fatec.gansoware.dominio.FTCGWPalestrante;
import br.gov.sp.fatec.gansoware.nucleo.necessario.FTCGWConexaoBase;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class FTCGWDAOPalestra extends FTCGWDAOAbstrato {

    @Override
    public void ftcgwSalvar(FTCGWEntidadeDominio ftcgw_entidade) throws Exception {

        assert (ftcgw_entidade != null);

        final FTCGWPalestra ftcgw_foco_palestra = (FTCGWPalestra) ftcgw_entidade;
        final StringBuilder ftcgw_query = new StringBuilder();
        PreparedStatement ftcgw_indagar_preparo = null;

        this.ftcgw_conexao.ftcgwPromoverConexao('f');

        ftcgw_query.append("INSERT INTO DFTCGWPalestra(ftcgwidentificador, ftcgwlocalizacao, ftcgwdurabilidade, ");
        ftcgw_query.append("ftcgwocasiao, ftcgwtitulo, ftcgwexecucao, ftcgwpalestrante) VALUES (?, ?, ?, ?, ?, ?, ?)");

        ftcgw_indagar_preparo = this.ftcgw_conexao.getFtcgw_via_conectiva().prepareStatement(ftcgw_query.toString());
        ftcgw_indagar_preparo.setLong(1, ftcgw_foco_palestra.getFtgcw_id());
        ftcgw_indagar_preparo.setString(2, ftcgw_foco_palestra.getFtcgw_localizacao());
        ftcgw_indagar_preparo.setShort(3, ftcgw_foco_palestra.getFtcgw_durabilidade());
        ftcgw_indagar_preparo.setString(4, ftcgw_foco_palestra.getFtcgw_ocasiao());
        ftcgw_indagar_preparo.setString(5, ftcgw_foco_palestra.getFtcgw_titulo());
        ftcgw_indagar_preparo.setBoolean(6, ftcgw_foco_palestra.isFtcgw_execucao());
        ftcgw_indagar_preparo.setLong(7, ftcgw_foco_palestra.getFtcgw_palestrante().getFtgcw_id());

        final int ftcgw_codoperativo = ftcgw_indagar_preparo.executeUpdate();
        ftcgw_indagar_preparo.close();

        this.ftcgw_conexao.ftcgwFinalizarConexao();
    }

    @Override
    public void ftcgwAlterar(FTCGWEntidadeDominio ftcgw_entidade) throws Exception {

        assert (ftcgw_entidade != null);

        final FTCGWPalestra ftcgw_foco_palestra = (FTCGWPalestra) ftcgw_entidade;
        final StringBuilder ftcgw_query = new StringBuilder();
        PreparedStatement ftcgw_indagar_preparo = null;

        this.ftcgw_conexao.ftcgwPromoverConexao('f');

        ftcgw_query.append("UPDATE DFTCGWPalestranta SET ftcgwlocalizacao=?, ftcgwdurabilidade=?, ftcgwocasiao=?, ");
        ftcgw_query.append("ftcgwtitulo=?, ftcgwexecucao=?, ftcgwpalestrante=? WHERE ftcgwidentificador=?");

        ftcgw_indagar_preparo = this.ftcgw_conexao.getFtcgw_via_conectiva().prepareStatement(ftcgw_query.toString());
        ftcgw_indagar_preparo.setString(1, ftcgw_foco_palestra.getFtcgw_localizacao());
        ftcgw_indagar_preparo.setShort(2, ftcgw_foco_palestra.getFtcgw_durabilidade());
        ftcgw_indagar_preparo.setString(3, ftcgw_foco_palestra.getFtcgw_ocasiao());
        ftcgw_indagar_preparo.setString(4, ftcgw_foco_palestra.getFtcgw_titulo());
        ftcgw_indagar_preparo.setBoolean(5, ftcgw_foco_palestra.isFtcgw_execucao());
        ftcgw_indagar_preparo.setLong(6, ftcgw_foco_palestra.getFtcgw_palestrante().getFtgcw_id());
        ftcgw_indagar_preparo.setLong(7, ftcgw_foco_palestra.getFtgcw_id());

        final int ftcgw_codoperativo = ftcgw_indagar_preparo.executeUpdate();
        ftcgw_indagar_preparo.close();

        this.ftcgw_conexao.ftcgwFinalizarConexao();
    }

    @Override
    public void ftcgwExcluir(FTCGWEntidadeDominio ftcgw_entidade) throws Exception {
    }

    @Override
    public List<FTCGWEntidadeDominio> ftcgwConsultar(FTCGWEntidadeDominio ftcgw_entidade) throws Exception {
        assert (ftcgw_entidade != null);

        final FTCGWPalestra ftcgw_foco_palestra = (FTCGWPalestra) ftcgw_entidade;
        final StringBuilder ftcgw_query = new StringBuilder();
        PreparedStatement ftcgw_indagar_preparo = null;

        this.ftcgw_conexao.ftcgwPromoverConexao('f');

        if (ftcgw_foco_palestra.getFtcgw_titulo() == null || ftcgw_foco_palestra.getFtcgw_titulo().length() <= 4 || ftcgw_foco_palestra.getFtcgw_titulo().equals("") || ftcgw_foco_palestra.getFtgcw_id() <= 0) {
            ftcgw_query.append("SELECT * FROM DFTCGWPalestra");
            ftcgw_indagar_preparo = this.ftcgw_conexao.getFtcgw_via_conectiva().prepareStatement(ftcgw_query.toString());
            final ResultSet ftcgw_indagacao_provisionada = ftcgw_indagar_preparo.executeQuery();

            if (ftcgw_indagacao_provisionada.isBeforeFirst()) {
                final List<FTCGWEntidadeDominio> ftcgw_palestras_viabilizadas = new ArrayList<>();
                while (ftcgw_indagacao_provisionada.next()) {
                    final FTCGWPalestra ftcgw_palestra_ciclica = new FTCGWPalestra();
                    ftcgw_palestra_ciclica.setFtcgw_durabilidade(ftcgw_indagacao_provisionada.getShort("ftcgwdurabilidade"));
                    ftcgw_palestra_ciclica.setFtcgw_localizacao(ftcgw_indagacao_provisionada.getString("ftcgwlocalizacao"));
                    ftcgw_palestra_ciclica.setFtcgw_ocasiao(ftcgw_indagacao_provisionada.getString("ftcgwocasiao"));
                    ftcgw_palestra_ciclica.setFtcgw_titulo(ftcgw_indagacao_provisionada.getString("ftcgwtitulo"));
                    ftcgw_palestra_ciclica.setFtcgw_execucao(ftcgw_indagacao_provisionada.getBoolean("ftcgwexecucao"));

                    final FTCGWPalestrante ftcgw_palestrante_propicio = new FTCGWPalestrante();
                    ftcgw_palestrante_propicio.setFtgcw_id(ftcgw_indagacao_provisionada.getShort("ftcgwpalestrante"));
                    ftcgw_palestra_ciclica.setFtcgw_palestrante(ftcgw_palestrante_propicio);

                    ftcgw_palestra_ciclica.setFtgcw_id(ftcgw_indagacao_provisionada.getShort("ftcgwidentificador"));
                    ftcgw_palestras_viabilizadas.add(ftcgw_palestra_ciclica);
                }
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return ftcgw_palestras_viabilizadas;
            } else {
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return null;
            }

        } else if (ftcgw_foco_palestra.getFtcgw_titulo() != null && ftcgw_foco_palestra.getFtcgw_titulo().length() > 4) {
            ftcgw_query.append("SELECT * FROM DFTCGWPalestra WHERE ftcgwtitulo=?");
            ftcgw_indagar_preparo = this.ftcgw_conexao.getFtcgw_via_conectiva().prepareStatement(ftcgw_query.toString());
            ftcgw_indagar_preparo.setString(1, ftcgw_foco_palestra.getFtcgw_titulo());
            final ResultSet ftcgw_indagacao_provisionada = ftcgw_indagar_preparo.executeQuery();

            if (ftcgw_indagacao_provisionada.isBeforeFirst()) {
                final List<FTCGWEntidadeDominio> ftcgw_palestras_viabilizadas = new ArrayList<>();
                while (ftcgw_indagacao_provisionada.next()) {
                    final FTCGWPalestra ftcgw_palestra_ciclica = new FTCGWPalestra();
                    ftcgw_palestra_ciclica.setFtcgw_durabilidade(ftcgw_indagacao_provisionada.getShort("ftcgwdurabilidade"));
                    ftcgw_palestra_ciclica.setFtcgw_localizacao(ftcgw_indagacao_provisionada.getString("ftcgwlocalizacao"));
                    ftcgw_palestra_ciclica.setFtcgw_ocasiao(ftcgw_indagacao_provisionada.getString("ftcgwocasiao"));
                    ftcgw_palestra_ciclica.setFtcgw_titulo(ftcgw_indagacao_provisionada.getString("ftcgwtitulo"));
                    ftcgw_palestra_ciclica.setFtcgw_execucao(ftcgw_indagacao_provisionada.getBoolean("ftcgwexecucao"));

                    final FTCGWPalestrante ftcgw_palestrante_propicio = new FTCGWPalestrante();
                    ftcgw_palestrante_propicio.setFtgcw_id(ftcgw_indagacao_provisionada.getLong("ftcgwpalestrante"));
                    ftcgw_palestra_ciclica.setFtcgw_palestrante(ftcgw_palestrante_propicio);

                    ftcgw_palestra_ciclica.setFtgcw_id(ftcgw_indagacao_provisionada.getShort("ftcgwidentificador"));
                    ftcgw_palestras_viabilizadas.add(ftcgw_palestra_ciclica);
                }
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return ftcgw_palestras_viabilizadas;
            } else {
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return null;
            }
        } else if (ftcgw_foco_palestra.getFtgcw_id() > 0) {
            ftcgw_query.append("SELECT * FROM DFTCGWPalestra WHERE ftcgwcontrole=?");
            ftcgw_indagar_preparo = this.ftcgw_conexao.getFtcgw_via_conectiva().prepareStatement(ftcgw_query.toString());
            ftcgw_indagar_preparo.setLong(1, ftcgw_foco_palestra.getFtgcw_id());
            final ResultSet ftcgw_indagacao_provisionada = ftcgw_indagar_preparo.executeQuery();

            if (ftcgw_indagacao_provisionada.isBeforeFirst()) {
                final List<FTCGWEntidadeDominio> ftcgw_palestras_viabilizadas = new ArrayList<>();
                while (ftcgw_indagacao_provisionada.next()) {
                    final FTCGWPalestra ftcgw_palestra_ciclica = new FTCGWPalestra();
                    ftcgw_palestra_ciclica.setFtcgw_durabilidade(ftcgw_indagacao_provisionada.getShort("ftcgwdurabilidade"));
                    ftcgw_palestra_ciclica.setFtcgw_localizacao(ftcgw_indagacao_provisionada.getString("ftcgwlocalizacao"));
                    ftcgw_palestra_ciclica.setFtcgw_ocasiao(ftcgw_indagacao_provisionada.getString("ftcgwocasiao"));
                    ftcgw_palestra_ciclica.setFtcgw_titulo(ftcgw_indagacao_provisionada.getString("ftcgwtitulo"));
                    ftcgw_palestra_ciclica.setFtcgw_execucao(ftcgw_indagacao_provisionada.getBoolean("ftcgwexecucao"));

                    final FTCGWPalestrante ftcgw_palestrante_propicio = new FTCGWPalestrante();
                    ftcgw_palestrante_propicio.setFtgcw_id(ftcgw_indagacao_provisionada.getLong("ftcgwpalestrante"));
                    ftcgw_palestra_ciclica.setFtcgw_palestrante(ftcgw_palestrante_propicio);

                    ftcgw_palestra_ciclica.setFtgcw_id(ftcgw_indagacao_provisionada.getLong("ftcgwidentificador"));
                    ftcgw_palestras_viabilizadas.add(ftcgw_palestra_ciclica);
                }
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return ftcgw_palestras_viabilizadas;
            } else {
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return null;
            }
        } else {
            this.ftcgw_conexao.ftcgwFinalizarConexao();
            return null;
        }
    }

    public FTCGWDAOPalestra(FTCGWConexaoBase ftcgw_conexao) {
        super(ftcgw_conexao);
    }
}

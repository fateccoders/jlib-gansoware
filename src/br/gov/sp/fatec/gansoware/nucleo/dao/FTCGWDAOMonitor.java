package br.gov.sp.fatec.gansoware.nucleo.dao;

import br.gov.sp.fatec.gansoware.dominio.FTCGWEntidadeDominio;
import br.gov.sp.fatec.gansoware.dominio.FTCGWMonitor;
import br.gov.sp.fatec.gansoware.dominio.FTCGWOscilacao;
import br.gov.sp.fatec.gansoware.nucleo.necessario.FTCGWConexaoBase;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class FTCGWDAOMonitor extends FTCGWDAOAbstrato {

    public FTCGWDAOMonitor(FTCGWConexaoBase ftcgw_conexao) {
        super(ftcgw_conexao);
    }

    @Override
    public void ftcgwSalvar(FTCGWEntidadeDominio ftcgw_entidade) throws Exception {

        assert (ftcgw_entidade != null);

        final FTCGWMonitor ftcgw_foco_monitor = (FTCGWMonitor) ftcgw_entidade;
        final StringBuilder ftcgw_query = new StringBuilder();
        PreparedStatement ftcgw_indagar_preparo = null;

        this.ftcgw_conexao.ftcgwPromoverConexao('f');

        ftcgw_query.append("INSERT INTO DFTCGWMonitor(ftcgwentrada, ftcgwoscilacao, ftcgwposicionamento, ftcgwdeslocamento) ");
        ftcgw_query.append("VALUES (?, ?, ?, ?)");

        ftcgw_indagar_preparo = this.ftcgw_conexao.getFtcgw_via_conectiva().prepareStatement(ftcgw_query.toString());
        ftcgw_indagar_preparo.setLong(1, ftcgw_foco_monitor.getFtgcw_id());
        ftcgw_indagar_preparo.setLong(2, ftcgw_foco_monitor.getFtcgw_computar_oscilacao().getFtgcw_id());
        ftcgw_indagar_preparo.setTimestamp(3, ftcgw_foco_monitor.getFtcgw_computar_datas_movimento()[0]);
        ftcgw_indagar_preparo.setTimestamp(4, ftcgw_foco_monitor.getFtcgw_computar_datas_movimento()[1]);

        final int ftcgw_codoperativo = ftcgw_indagar_preparo.executeUpdate();
        ftcgw_indagar_preparo.close();

        this.ftcgw_conexao.ftcgwFinalizarConexao();
    }

    @Override
    public void ftcgwAlterar(FTCGWEntidadeDominio ftcgw_entidade) throws Exception {

        assert (ftcgw_entidade != null);

        final FTCGWMonitor ftcgw_foco_monitor = (FTCGWMonitor) ftcgw_entidade;
        final StringBuilder ftcgw_query = new StringBuilder();
        PreparedStatement ftcgw_indagar_preparo = null;

        this.ftcgw_conexao.ftcgwPromoverConexao('f');

        ftcgw_query.append("UPDATE DFTCGWMonitor SET ftcgwoscilacao=?, ftcgwposicionamento=?,");
        ftcgw_query.append(" ftcgwdeslocamento=? WHERE ftcgwentrada=?");

        ftcgw_indagar_preparo = this.ftcgw_conexao.getFtcgw_via_conectiva().prepareStatement(ftcgw_query.toString());
        ftcgw_indagar_preparo.setLong(1, ftcgw_foco_monitor.getFtgcw_id());
        ftcgw_indagar_preparo.setTimestamp(2, ftcgw_foco_monitor.getFtcgw_computar_datas_movimento()[0]);
        ftcgw_indagar_preparo.setTimestamp(3, ftcgw_foco_monitor.getFtcgw_computar_datas_movimento()[1]);
        ftcgw_indagar_preparo.setLong(4, ftcgw_foco_monitor.getFtcgw_computar_oscilacao().getFtgcw_id());

        final int ftcgw_codoperativo = ftcgw_indagar_preparo.executeUpdate();
        ftcgw_indagar_preparo.close();

        this.ftcgw_conexao.ftcgwFinalizarConexao();
    }

    @Override
    public void ftcgwExcluir(FTCGWEntidadeDominio ftcgw_entidade) throws Exception {
    }

    @Override
    public List<FTCGWEntidadeDominio> ftcgwConsultar(FTCGWEntidadeDominio ftcgw_entidade) throws Exception {
        assert (ftcgw_entidade != null);

        FTCGWMonitor ftcgw_foco_monitor = (FTCGWMonitor) ftcgw_entidade;
        final StringBuilder ftcgw_query = new StringBuilder();
        PreparedStatement ftcgw_indagar_preparo = null;

        this.ftcgw_conexao.ftcgwPromoverConexao('f');

        if (ftcgw_foco_monitor.getFtgcw_id() <= 0) {
            ftcgw_query.append("SELECT * FROM DFTCGWMonitor");
            ftcgw_indagar_preparo = this.ftcgw_conexao.getFtcgw_via_conectiva().prepareStatement(ftcgw_query.toString());
            final ResultSet ftcgw_indagacao_provisionada = ftcgw_indagar_preparo.executeQuery();

            if (ftcgw_indagacao_provisionada.isBeforeFirst()) {
                final List<FTCGWEntidadeDominio> ftcgw_monitores_viabilizados = new ArrayList<>();
                while (ftcgw_indagacao_provisionada.next()) {
                    final FTCGWMonitor ftcgw_monitor_ciclico = new FTCGWMonitor();

                    ftcgw_monitor_ciclico.setFtgcw_id(ftcgw_indagacao_provisionada.getLong("ftcgwentrada"));

                    final FTCGWOscilacao ftcgw_oscilacao_iterada = new FTCGWOscilacao();
                    ftcgw_oscilacao_iterada.setFtgcw_id(ftcgw_indagacao_provisionada.getLong("ftcgwoscilacao"));
                    ftcgw_monitor_ciclico.setFtcgw_computar_oscilacao(ftcgw_oscilacao_iterada);//**

                    final Timestamp[] ftcgw_movs_por_graduando = new Timestamp[2];
                    ftcgw_movs_por_graduando[0] = ftcgw_indagacao_provisionada.getTimestamp("ftcgwposicionamento");
                    ftcgw_movs_por_graduando[1] = ftcgw_indagacao_provisionada.getTimestamp("ftcgwdeslocamento");
                    ftcgw_monitor_ciclico.setFtcgw_computar_datas_movimento(ftcgw_movs_por_graduando);

                    ftcgw_monitores_viabilizados.add(ftcgw_monitor_ciclico);
                }
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return ftcgw_monitores_viabilizados;
            } else {
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return null;
            }
        } else {
            ftcgw_query.append("SELECT * FROM DFTCGWMonitor WHERE ftcgwentrada=?");
            ftcgw_indagar_preparo = this.ftcgw_conexao.getFtcgw_via_conectiva().prepareStatement(ftcgw_query.toString());
            ftcgw_indagar_preparo.setLong(1, ftcgw_foco_monitor.getFtgcw_id());
            final ResultSet ftcgw_indagacao_provisionada = ftcgw_indagar_preparo.executeQuery();

            if (ftcgw_indagacao_provisionada.isBeforeFirst()) {
                final List<FTCGWEntidadeDominio> ftcgw_monitores_viabilizados = new ArrayList<>();
                while (ftcgw_indagacao_provisionada.next()) {
                    final FTCGWMonitor ftcgw_monitor_ciclico = new FTCGWMonitor();

                    ftcgw_monitor_ciclico.setFtgcw_id(ftcgw_indagacao_provisionada.getLong("ftcgwentrada"));

                    final FTCGWOscilacao ftcgw_oscilacao_iterada = new FTCGWOscilacao();
                    ftcgw_oscilacao_iterada.setFtgcw_id(ftcgw_indagacao_provisionada.getLong("ftcgwoscilacao"));
                    ftcgw_monitor_ciclico.setFtcgw_computar_oscilacao(ftcgw_oscilacao_iterada);//**

                    final Timestamp[] ftcgw_movs_por_graduando = new Timestamp[2];
                    ftcgw_movs_por_graduando[0] = ftcgw_indagacao_provisionada.getTimestamp("ftcgwposicionamento");
                    ftcgw_movs_por_graduando[1] = ftcgw_indagacao_provisionada.getTimestamp("ftcgwdeslocamento");
                    ftcgw_monitor_ciclico.setFtcgw_computar_datas_movimento(ftcgw_movs_por_graduando);

                    ftcgw_monitores_viabilizados.add(ftcgw_monitor_ciclico);
                }
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return ftcgw_monitores_viabilizados;
            } else {
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return null;
            }
        }
    }

}

package br.gov.sp.fatec.gansoware.nucleo.dao;

import br.gov.sp.fatec.gansoware.dominio.FTCGWEntidadeDominio;
import br.gov.sp.fatec.gansoware.dominio.FTCGWGraduando;
import br.gov.sp.fatec.gansoware.dominio.FTCGWOscilacao;
import br.gov.sp.fatec.gansoware.dominio.FTCGWPalestra;
import br.gov.sp.fatec.gansoware.nucleo.necessario.FTCGWConexaoBase;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class FTCGWDAOOscilacao extends FTCGWDAOAbstrato {

    @Override
    public void ftcgwSalvar(FTCGWEntidadeDominio ftcgw_entidade) throws Exception {
        assert (ftcgw_entidade != null);

        final FTCGWOscilacao ftcgw_foco_oscilacao = (FTCGWOscilacao) ftcgw_entidade;
        final StringBuilder ftcgw_query = new StringBuilder();
        PreparedStatement ftcgw_indagar_preparo = null;

        this.ftcgw_conexao.ftcgwPromoverConexao('f');

        ftcgw_query.append("INSERT INTO DFTCGWOscilacao(ftcgwgatilho, ftcgwpalestra, ftcgwaluno) ");
        ftcgw_query.append("VALUES (?, ?, ?)");

        ftcgw_indagar_preparo = this.ftcgw_conexao.getFtcgw_via_conectiva().prepareStatement(ftcgw_query.toString());
        ftcgw_indagar_preparo.setLong(1, ftcgw_foco_oscilacao.getFtgcw_id());
        ftcgw_indagar_preparo.setLong(2, ftcgw_foco_oscilacao.getFtcgw_associacao_nivelar_palestra().getFtgcw_id());
        ftcgw_indagar_preparo.setLong(3, ftcgw_foco_oscilacao.getFtcw_associacao_nivelar_aluno().getFtgcw_id());

        final int ftcgw_codoperativo = ftcgw_indagar_preparo.executeUpdate();
        ftcgw_indagar_preparo.close();

        this.ftcgw_conexao.ftcgwFinalizarConexao();
    }

    @Override
    public void ftcgwAlterar(FTCGWEntidadeDominio ftcgw_entidade) throws Exception {
    }

    @Override
    public void ftcgwExcluir(FTCGWEntidadeDominio ftcgw_entidade) throws Exception {
        assert (ftcgw_entidade != null);

        final FTCGWOscilacao ftcgw_foco_oscilacao = (FTCGWOscilacao) ftcgw_entidade;
        final StringBuilder ftcgw_query = new StringBuilder();
        PreparedStatement ftcgw_indagar_preparo = null;

        this.ftcgw_conexao.ftcgwPromoverConexao('f');

        ftcgw_query.append("DELETE FROM DFTCGWOscilacao ");
        ftcgw_query.append("WHERE ftcgwgatilho=?");

        ftcgw_indagar_preparo = this.ftcgw_conexao.getFtcgw_via_conectiva().prepareStatement(ftcgw_query.toString());
        ftcgw_indagar_preparo.setLong(1, ftcgw_foco_oscilacao.getFtgcw_id());

        final int ftcgw_codoperativo = ftcgw_indagar_preparo.executeUpdate();
        ftcgw_indagar_preparo.close();

        this.ftcgw_conexao.ftcgwFinalizarConexao();
    }

    @Override
    public List<FTCGWEntidadeDominio> ftcgwConsultar(FTCGWEntidadeDominio ftcgw_entidade) throws Exception {
        assert (ftcgw_entidade != null);

        FTCGWOscilacao ftcgw_foco_oscilacao = (FTCGWOscilacao) ftcgw_entidade;
        final StringBuilder ftcgw_query = new StringBuilder();
        PreparedStatement ftcgw_indagar_preparo = null;

        this.ftcgw_conexao.ftcgwPromoverConexao('f');

        if (ftcgw_foco_oscilacao.getFtgcw_id() <= 0 && ftcgw_foco_oscilacao.getFtcgw_associacao_nivelar_palestra() == null) {
            ftcgw_query.append("SELECT * FROM DFTCGWOscilacao");
            ftcgw_indagar_preparo = this.ftcgw_conexao.getFtcgw_via_conectiva().prepareStatement(ftcgw_query.toString());
            final ResultSet ftcgw_indagacao_provisionada = ftcgw_indagar_preparo.executeQuery();

            if (ftcgw_indagacao_provisionada.isBeforeFirst()) {
                final List<FTCGWEntidadeDominio> ftcgw_oscilacoes_viabilizadas = new ArrayList<>();
                while (ftcgw_indagacao_provisionada.next()) {
                    final FTCGWOscilacao ftcgw_oscilacao_ciclica = new FTCGWOscilacao();

                    ftcgw_oscilacao_ciclica.setFtgcw_id(ftcgw_indagacao_provisionada.getLong("ftcgwgatilho"));

                    final FTCGWPalestra ftcgw_palestra_iterada = new FTCGWPalestra();
                    ftcgw_palestra_iterada.setFtgcw_id(ftcgw_indagacao_provisionada.getLong("ftcgwpalestra"));
                    ftcgw_oscilacao_ciclica.setFtcgw_associacao_nivelar_palestra(ftcgw_palestra_iterada);//**

                    final FTCGWGraduando ftcgw_graduando_iterado = new FTCGWGraduando();
                    ftcgw_graduando_iterado.setFtgcw_id(ftcgw_indagacao_provisionada.getLong("ftcgwaluno"));
                    ftcgw_oscilacao_ciclica.setFtcw_associacao_nivelar_aluno(ftcgw_graduando_iterado);//**

                    ftcgw_oscilacoes_viabilizadas.add(ftcgw_oscilacao_ciclica);
                }
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return ftcgw_oscilacoes_viabilizadas;
            } else {
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return null;
            }
        } else {
            ftcgw_query.append("SELECT * FROM DFTCGWOscilacao WHERE ftcgwpalestra=?");
            ftcgw_indagar_preparo = this.ftcgw_conexao.getFtcgw_via_conectiva().prepareStatement(ftcgw_query.toString());
            ftcgw_indagar_preparo.setLong(1, ftcgw_foco_oscilacao.getFtcgw_associacao_nivelar_palestra().getFtgcw_id());
            final ResultSet ftcgw_indagacao_provisionada = ftcgw_indagar_preparo.executeQuery();

            if (ftcgw_indagacao_provisionada.isBeforeFirst()) {
                final List<FTCGWEntidadeDominio> ftcgw_oscilacoes_viabilizadas = new ArrayList<>();
                while (ftcgw_indagacao_provisionada.next()) {
                    final FTCGWOscilacao ftcgw_oscilacao_ciclica = new FTCGWOscilacao();

                    ftcgw_oscilacao_ciclica.setFtgcw_id(ftcgw_indagacao_provisionada.getLong("ftcgwgatilho"));

                    final FTCGWPalestra ftcgw_palestra_iterada = new FTCGWPalestra();
                    ftcgw_palestra_iterada.setFtgcw_id(ftcgw_indagacao_provisionada.getLong("ftcgwpalestra"));
                    ftcgw_oscilacao_ciclica.setFtcgw_associacao_nivelar_palestra(ftcgw_palestra_iterada);//**

                    final FTCGWGraduando ftcgw_graduando_iterado = new FTCGWGraduando();
                    ftcgw_graduando_iterado.setFtgcw_id(ftcgw_indagacao_provisionada.getLong("ftcgwaluno"));
                    ftcgw_oscilacao_ciclica.setFtcw_associacao_nivelar_aluno(ftcgw_graduando_iterado);//**

                    ftcgw_oscilacoes_viabilizadas.add(ftcgw_oscilacao_ciclica);
                }
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return ftcgw_oscilacoes_viabilizadas;
            } else {
                this.ftcgw_conexao.ftcgwFinalizarConexao();
                return null;
            }
        }

    }

    public FTCGWDAOOscilacao(FTCGWConexaoBase ftcgw_conexao) {
        super(ftcgw_conexao);
    }
}

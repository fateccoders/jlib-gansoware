package br.gov.sp.fatec.gansoware.nucleo;

import br.gov.sp.fatec.gansoware.dominio.FTCGWEntidadeDominio;
import br.gov.sp.fatec.gansoware.nucleo.aplicacao.FTCGWRetornoResultado;

import java.util.List;

public interface FTCGWIFacade {

    public FTCGWRetornoResultado ftcgwSalvar(final FTCGWEntidadeDominio ftcgw_entidade);

    public FTCGWRetornoResultado ftcgwAlterar(final FTCGWEntidadeDominio ftcgw_entidade);

    public FTCGWRetornoResultado ftcgwExcluir(final FTCGWEntidadeDominio ftcgw_entidade);

    public FTCGWRetornoResultado ftcgwConsultar(final FTCGWEntidadeDominio ftcgw_entidade);

    public FTCGWRetornoResultado ftcgwVisualizar(final FTCGWEntidadeDominio ftcgw_entidade);
}

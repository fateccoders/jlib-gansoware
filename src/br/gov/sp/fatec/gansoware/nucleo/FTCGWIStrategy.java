package br.gov.sp.fatec.gansoware.nucleo;

import br.gov.sp.fatec.gansoware.dominio.FTCGWEntidadeDominio;

public interface FTCGWIStrategy {

    public String processar(FTCGWEntidadeDominio ftcgw_entidade);
}

package br.gov.sp.fatec.gansoware.nucleo.necessario;

import java.sql.Connection;
import java.sql.DriverManager;

public final class FTCGWConexaoBase {

    private Connection ftcgw_via_conectiva = null;

    public Connection getFtcgw_via_conectiva() {
        return ftcgw_via_conectiva;
    }

    public void setFtcgw_via_conectiva(Connection ftcgw_via_conectiva) {
        this.ftcgw_via_conectiva = ftcgw_via_conectiva;
    }

    public final void ftcgwPromoverConexao(final char ftcgw_sgbd) throws Exception {
        switch (ftcgw_sgbd) {
            case 'f': {
                if (this.ftcgw_via_conectiva != null) {
                    break;
                }

                final FTCGWUConexaoBase ftcgw_conexao_base = new FTCGWUConexaoBase("org.mariadb.jdbc.Driver", "jdbc:mysql://127.0.0.1:3306/BFTCGW1", "ewe", "www");
                Class.forName(ftcgw_conexao_base.getFtcgw_driver());
                this.ftcgw_via_conectiva = DriverManager.getConnection(ftcgw_conexao_base.getFtcgw_base(), ftcgw_conexao_base.getFtcgw_usuario(), ftcgw_conexao_base.getFtcgw_senha());
            }
            break;
        }
    }

    public final void ftcgwFinalizarConexao() throws Exception {
        if (this.ftcgw_via_conectiva != null) {
            this.ftcgw_via_conectiva.close();
        }
    }
}

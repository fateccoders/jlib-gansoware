package br.gov.sp.fatec.gansoware.nucleo.necessario;

public class FTCGWUConexaoBase {

    private String ftcgw_driver = null;
    private String ftcgw_base = null;
    private String ftcgw_usuario = null;
    private String ftcgw_senha = null;

    public String getFtcgw_driver() {
        return ftcgw_driver;
    }

    public void setFtcgw_driver(String ftcgw_driver) {
        this.ftcgw_driver = ftcgw_driver;
    }

    public String getFtcgw_base() {
        return ftcgw_base;
    }

    public void setFtcgw_base(String ftcgw_base) {
        this.ftcgw_base = ftcgw_base;
    }

    public String getFtcgw_usuario() {
        return ftcgw_usuario;
    }

    public void setFtcgw_usuario(String ftcgw_usuario) {
        this.ftcgw_usuario = ftcgw_usuario;
    }

    public String getFtcgw_senha() {
        return ftcgw_senha;
    }

    public void setFtcgw_senha(String ftcgw_senha) {
        this.ftcgw_senha = ftcgw_senha;
    }

    public FTCGWUConexaoBase(String ftcgw_driver, String ftcgw_base, String ftcgw_usuario, String ftcgw_senha) {
        this.ftcgw_driver = ftcgw_driver;
        this.ftcgw_base = ftcgw_base;
        this.ftcgw_usuario = ftcgw_usuario;
        this.ftcgw_senha = ftcgw_senha;
    }

}

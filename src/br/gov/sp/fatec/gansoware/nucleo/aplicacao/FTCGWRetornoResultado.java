package br.gov.sp.fatec.gansoware.nucleo.aplicacao;

import br.gov.sp.fatec.gansoware.dominio.FTCGWEntidadeDominio;
import java.util.List;

public class FTCGWRetornoResultado extends FTCGWEntidadeAplicacao {

    private String ftcgw_mensagem_informativa;
    private List<FTCGWEntidadeDominio> ftcgw_entidades;

    public String getFtcgw_mensagem_informativa() {
        return ftcgw_mensagem_informativa;
    }

    public void setFtcgw_mensagem_informativa(String ftcgw_mensagem_informativa) {
        this.ftcgw_mensagem_informativa = ftcgw_mensagem_informativa;
    }

    public List<FTCGWEntidadeDominio> getFtcgw_entidades() {
        return ftcgw_entidades;
    }

    public void setFtcgw_entidades(List<FTCGWEntidadeDominio> ftcgw_entidades) {
        this.ftcgw_entidades = ftcgw_entidades;
    }

}

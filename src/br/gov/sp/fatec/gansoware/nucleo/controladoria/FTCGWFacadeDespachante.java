package br.gov.sp.fatec.gansoware.nucleo.controladoria;

import br.gov.sp.fatec.gansoware.dominio.FTCGWEntidadeDominio;
import br.gov.sp.fatec.gansoware.dominio.FTCGWGraduando;
import br.gov.sp.fatec.gansoware.dominio.FTCGWMonitor;
import br.gov.sp.fatec.gansoware.dominio.FTCGWOscilacao;
import br.gov.sp.fatec.gansoware.dominio.FTCGWPalestra;
import br.gov.sp.fatec.gansoware.dominio.FTCGWPalestrante;
import br.gov.sp.fatec.gansoware.nucleo.FTCGWIDAO;
import br.gov.sp.fatec.gansoware.nucleo.aplicacao.FTCGWRetornoResultado;
import br.gov.sp.fatec.gansoware.nucleo.dao.FTCGWDAOGraduando;
import br.gov.sp.fatec.gansoware.nucleo.dao.FTCGWDAOMonitor;
import br.gov.sp.fatec.gansoware.nucleo.dao.FTCGWDAOOscilacao;
import br.gov.sp.fatec.gansoware.nucleo.dao.FTCGWDAOPalestra;
import br.gov.sp.fatec.gansoware.nucleo.dao.FTCGWDAOPalestrante;
import br.gov.sp.fatec.gansoware.nucleo.necessario.FTCGWConexaoBase;
import java.util.HashMap;

public class FTCGWFacadeDespachante extends FTCGWFacadeAbstrato {

    public FTCGWFacadeDespachante() {
        this.ftcgwPromoverEspecificacaoDoMapeamentoOficial('d');
    }

    @Override
    public FTCGWRetornoResultado ftcgwSalvar(FTCGWEntidadeDominio ftcgw_entidade) {
        assert (ftcgw_entidade != null && this.ftcgw_mapeamento_oficial_daos != null);

        FTCGWRetornoResultado ftgw_processamento_responsivo = new FTCGWRetornoResultado();
        FTCGWIDAO ftcgw_minerar_dao_objetivo = this.ftcgw_mapeamento_oficial_daos.get(ftcgw_entidade.getClass().getName().toLowerCase());
        try {
            ftcgw_minerar_dao_objetivo.ftcgwSalvar(ftcgw_entidade);
            ftgw_processamento_responsivo.setFtcgw_mensagem_informativa("FTCGWS (LIB\\Salvar): Execucao concretizada    *" + ftcgw_minerar_dao_objetivo.toString());
        } catch (Exception ftcgw_excecao) {
            ftcgw_excecao.printStackTrace();
        }
        return ftgw_processamento_responsivo;
    }

    @Override
    public FTCGWRetornoResultado ftcgwAlterar(FTCGWEntidadeDominio ftcgw_entidade) {
        assert (ftcgw_entidade != null && this.ftcgw_mapeamento_oficial_daos != null);

        FTCGWRetornoResultado ftgw_processamento_responsivo = new FTCGWRetornoResultado();
        FTCGWIDAO ftcgw_minerar_dao_objetivo = this.ftcgw_mapeamento_oficial_daos.get(ftcgw_entidade.getClass().getName().toLowerCase());
        try {
            ftcgw_minerar_dao_objetivo.ftcgwAlterar(ftcgw_entidade);
            ftgw_processamento_responsivo.setFtcgw_mensagem_informativa("FTCGWS (LIB\\Alterar): Execucao concretizada    *" + ftcgw_minerar_dao_objetivo.toString());
        } catch (Exception ftcgw_excecao) {
            ftcgw_excecao.printStackTrace();
        }
        return ftgw_processamento_responsivo;
    }

    @Override
    public FTCGWRetornoResultado ftcgwExcluir(FTCGWEntidadeDominio ftcgw_entidade) {
        assert (ftcgw_entidade != null && this.ftcgw_mapeamento_oficial_daos != null);

        FTCGWRetornoResultado ftgw_processamento_responsivo = new FTCGWRetornoResultado();
        FTCGWIDAO ftcgw_minerar_dao_objetivo = this.ftcgw_mapeamento_oficial_daos.get(ftcgw_entidade.getClass().getName().toLowerCase());
        try {
            ftcgw_minerar_dao_objetivo.ftcgwExcluir(ftcgw_entidade);
            ftgw_processamento_responsivo.setFtcgw_mensagem_informativa("FTCGWS (LIB\\Excluir): Execucao concretizada    *" + ftcgw_minerar_dao_objetivo.toString());
        } catch (Exception ftcgw_excecao) {
            ftcgw_excecao.printStackTrace();
        }
        return ftgw_processamento_responsivo;
    }

    @Override
    public FTCGWRetornoResultado ftcgwConsultar(FTCGWEntidadeDominio ftcgw_entidade) {
        assert (ftcgw_entidade != null && this.ftcgw_mapeamento_oficial_daos != null);

        FTCGWRetornoResultado ftgw_processamento_responsivo = new FTCGWRetornoResultado();
        FTCGWIDAO ftcgw_minerar_dao_objetivo = this.ftcgw_mapeamento_oficial_daos.get(ftcgw_entidade.getClass().getName().toLowerCase());
        try {
            ftgw_processamento_responsivo.setFtcgw_entidades(ftcgw_minerar_dao_objetivo.ftcgwConsultar(ftcgw_entidade));
            ftgw_processamento_responsivo.setFtcgw_mensagem_informativa("FTCGWS (LIB\\Consultar): Execucao concretizada    *" + ftcgw_minerar_dao_objetivo.toString());
        } catch (Exception ftcgw_excecao) {
            ftcgw_excecao.printStackTrace();
        }

        return ftgw_processamento_responsivo;
    }

    @Override
    public FTCGWRetornoResultado ftcgwVisualizar(FTCGWEntidadeDominio ftcgw_entidade) {
        return null;
    }

    @Override
    protected void ftcgwPromoverEspecificacaoDoMapeamentoOficial(char ftcgw_direcao) {
        if (ftcgw_direcao == '\0' || ftcgw_direcao == '0') {
            return;
        }

        switch (ftcgw_direcao) {
            case 'd': {
                this.ftcgw_mapeamento_oficial_daos = new HashMap<>();
                this.ftcgw_conexao_estrutural_em_base = new FTCGWConexaoBase();
                this.ftcgw_mapeamento_oficial_daos.put(FTCGWPalestrante.class.getName().toLowerCase(), new FTCGWDAOPalestrante(this.ftcgw_conexao_estrutural_em_base));
                this.ftcgw_mapeamento_oficial_daos.put(FTCGWPalestra.class.getName().toLowerCase(), new FTCGWDAOPalestra(this.ftcgw_conexao_estrutural_em_base));
                this.ftcgw_mapeamento_oficial_daos.put(FTCGWGraduando.class.getName().toLowerCase(), new FTCGWDAOGraduando(this.ftcgw_conexao_estrutural_em_base));
                this.ftcgw_mapeamento_oficial_daos.put(FTCGWOscilacao.class.getName().toLowerCase(), new FTCGWDAOOscilacao(this.ftcgw_conexao_estrutural_em_base));
                this.ftcgw_mapeamento_oficial_daos.put(FTCGWMonitor.class.getName().toLowerCase(), new FTCGWDAOMonitor(this.ftcgw_conexao_estrutural_em_base));
            }
            break;

            default: {
                return;
            }
        }
    }

}

package br.gov.sp.fatec.gansoware.nucleo.controladoria;

import br.gov.sp.fatec.gansoware.nucleo.FTCGWIDAO;
import br.gov.sp.fatec.gansoware.nucleo.FTCGWIFacade;
import br.gov.sp.fatec.gansoware.nucleo.necessario.FTCGWConexaoBase;
import java.util.HashMap;

public abstract class FTCGWFacadeAbstrato implements FTCGWIFacade {

    protected FTCGWConexaoBase ftcgw_conexao_estrutural_em_base = null;
    protected HashMap<String, FTCGWIDAO> ftcgw_mapeamento_oficial_daos = null;

    protected abstract void ftcgwPromoverEspecificacaoDoMapeamentoOficial(final char ftcgw_direcao);

}

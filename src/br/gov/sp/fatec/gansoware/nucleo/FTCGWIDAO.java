package br.gov.sp.fatec.gansoware.nucleo;

import br.gov.sp.fatec.gansoware.dominio.FTCGWEntidadeDominio;
import java.util.List;

public interface FTCGWIDAO {

    public void ftcgwSalvar(FTCGWEntidadeDominio ftcgw_entidade) throws Exception;

    public void ftcgwAlterar(FTCGWEntidadeDominio ftcgw_entidade) throws Exception;

    public void ftcgwExcluir(FTCGWEntidadeDominio ftcgw_entidade) throws Exception;

    public List<FTCGWEntidadeDominio> ftcgwConsultar(FTCGWEntidadeDominio ftcgw_entidade) throws Exception;
}

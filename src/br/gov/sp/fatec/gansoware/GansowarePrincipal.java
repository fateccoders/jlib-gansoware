package br.gov.sp.fatec.gansoware;

public class GansowarePrincipal {

    public static void main(String[] args) throws Exception {
        /* TESTE EXECUTAVEL
        1-
        final FTCGWGraduando ftcgw_novo_graduando = new FTCGWGraduando();
        ftcgw_novo_graduando.setFtgcw_id(12);
        ftcgw_novo_graduando.setFtcgw_nome("Matheus Henrique Silva");
        ftcgw_novo_graduando.setFtcgw_idade((short) 20);
        ftcgw_novo_graduando.setFtcgw_sexo((byte) 0);
        ftcgw_novo_graduando.setFtcgw_ra("148900872410093");
        FTCGWDAOGraduando ftcgw_dao_graduando = new FTCGWDAOGraduando(new FTCGWConexaoBase());
        ftcgw_dao_graduando.ftcgwSalvar(ftcgw_novo_graduando);
        2-
        final FTCGWPalestrante ftcgw_novo_palestrante = new FTCGWPalestrante();
        ftcgw_novo_palestrante.setFtgcw_id(15);
        ftcgw_novo_palestrante.setFtcgw_nome("Felicia Cenoura");
        ftcgw_novo_palestrante.setFtcgw_idade((short) 12);
        ftcgw_novo_palestrante.setFtcgw_sexo((byte) 1);
        ftcgw_novo_palestrante.setFtcgw_proposito("Vamos falar sobre os pobres animais que se tornam hamburgueres.");
        FTCGWDAOPalestrante ftcgw_dao_palestrante = new FTCGWDAOPalestrante(new FTCGWConexaoBase());
        ftcgw_dao_palestrante.ftcgwSalvar(ftcgw_novo_palestrante);
        3-
        final FTCGWPalestra nova_palestra = new FTCGWPalestra();
        nova_palestra.setFtgcw_id(3);
        nova_palestra.setFtcgw_localizacao("Sala Eventos");
        nova_palestra.setFtcgw_durabilidade((short) 65);
        nova_palestra.setFtcgw_ocasiao("2017-10-22");
        nova_palestra.setFtcgw_execucao(true);
        nova_palestra.setFtcgw_palestrante(ftcgw_novo_palestrante);
        nova_palestra.setFtcgw_titulo("Por que voce nao come seu cachorro tbm?");
        FTCGWDAOPalestra ftcgw_dao_palestra = new FTCGWDAOPalestra(new FTCGWConexaoBase());
        ftcgw_dao_palestra.ftcgwSalvar(nova_palestra);
        A-
        FTCGWFacadeDespachante fd = new FTCGWFacadeDespachante();
        for(FTCGWEntidadeDominio a : fd.ftcgwConsultar(new FTCGWGraduando()).getFtcgw_entidades())
        {
            final FTCGWGraduando pega = (FTCGWGraduando) a;
            System.out.println(pega.getFtcgw_nome());
        }
         */
    }
}

####create database BFTCGW;
####use BFTCGW;

create table if not exists DFTCGWGraduando(
	ftcgwcontrole smallint not null primary key,
	ftcgwnome varchar(40) not null,
	ftcgwsexo char not null,
	ftcgwidade tinyint not null,
	ftcgwra varchar(20) not null
);

create table if not exists DFTCGWPalestrante(
	ftcgwcontrole smallint not null primary key,
	ftcgwnome varchar(40) not null,
	ftcgwsexo char not null,
	ftcgwidade tinyint not null,
	ftcgwproposito text
);

create table if not exists DFTCGWPalestra(
	ftcgwidentificador tinyint not null primary key,
	ftcgwlocalizacao varchar(14) not null,
	ftcgwdurabilidade smallint not null,
	ftcgwocasiao date not null,
	ftcgwtitulo varchar(40) not null,
	ftcgwexecucao bool not null default false,
	ftcgwpalestrante smallint,
	constraint `aponta_palestrante` foreign key(ftcgwpalestrante) references DFTCGWPalestrante(ftcgwcontrole) on delete cascade on update restrict
);

create table if not exists DFTCGWOscilacao
(
	ftcgwgatilho bigint not null primary key,
	ftcgwpalestra tinyint,
	ftcgwaluno smallint,
	constraint `aponta_palestra` foreign key(ftcgwpalestra) references DFTCGWPalestra(ftcgwidentificador) on delete cascade on update restrict,
	constraint `aponta_aluno` foreign key(ftcgwaluno) references DFTCGWGraduando(ftcgwcontrole) on delete cascade on update restrict
);

create table if not exists DFTCGWMonitor
(
	ftcgwentrada bigint not null primary key,
	ftcgwoscilacao bigint,
	ftcgwposicionamento datetime,
	ftcgwdeslocamento datetime,
	constraint `aponta_oscilacao` foreign key(ftcgwoscilacao) references DFTCGWOscilacao(ftcgwgatilho) on delete cascade on update restrict 
);